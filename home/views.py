from django.shortcuts import render, redirect
from home.forms import Register


def home(request):
    active_nav = 'active'
    args = {'Home': active_nav}
    return render(request, "home/home.html", args)


def register(request):
    if request.method == 'POST':
        form = Register(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/home/login')
    else:
        form = Register()
        args = {'form': form}
        return render(request, "home/register.html", args)